const namespaced = true;

const state = {
  guest: null,
  url: "http://demo-api-vue.sanbercloud.com/api/v2",
};

const getters = {
  guest: state => state.guest,
  url: state => state.url,
};

const mutations = {
  SET_GUEST(state, payload) {
    state.guest = payload;
  },
};

const actions = {
};

export default { namespaced, state, getters, mutations, actions };
