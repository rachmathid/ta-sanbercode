import Vue from "vue";
import Vuex from "vuex";
import Auth from "./modules/Auth";
import Crud from './modules/Crud'

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    Auth,
    Crud
  },
});
